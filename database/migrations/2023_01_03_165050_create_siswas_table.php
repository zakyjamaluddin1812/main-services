<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->id();
            $table->string('tahun_ajaran');
            $table->string('nama_lengkap');
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('asal_sekolah');
            $table->string('nisn')->nullable();
            $table->bigInteger('nik_siswa');
            $table->bigInteger('nomor_kk');
            $table->string('jenis_kelamin');
            $table->bigInteger('anak_ke');
            $table->bigInteger('jumlah_saudara');
            $table->bigInteger('rt');
            $table->bigInteger('rw');
            $table->string('desa');
            $table->string('kecamatan');
            $table->string('kabupaten');
            $table->string('nama_ayah');
            $table->bigInteger('nik_ayah');
            $table->string('pekerjaan_ayah');
            $table->string('nama_ibu');
            $table->bigInteger('nik_ibu');
            $table->string('pekerjaan_ibu');
            $table->string('nama_wali')->nullable();
            $table->bigInteger('nik_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('nomor_hp')->nullable();
            $table->string('file_kk')->nullable();
            $table->string('file_ijazah')->nullable();
            $table->string('file_nisn')->nullable();
            $table->string('file_kps')->nullable();
            $table->string('file_pkh')->nullable();
            $table->string('file_foto')->nullable();
            $table->string('guru_perekom')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
};
