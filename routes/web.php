<?php

use App\Http\Controllers\Blog;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Formulir;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::any('/', function () {
//     return redirect('ppdb/form');
// });
Route::get('/', [Dashboard::class, 'index']);
Route::post('/form', [Formulir::class, 'addSiswa']);
Route::get('/ppdb', [Dashboard::class, 'ppdb']);
Route::get('/ppdb/form', [Formulir::class, 'ppdbForm']);
Route::get('/ppdb/finish/{id}', [Formulir::class, 'finish']);
Route::get('/ppdb/kk/{id}', [Formulir::class, 'ppdbKk']);
Route::get('/ppdb/ijazah/{id}', [Formulir::class, 'ppdbIjazah']);
Route::get('/ppdb/nisn/{id}', [Formulir::class, 'ppdbNisn']);
Route::get('/ppdb/kps/{id}', [Formulir::class, 'ppdbKps']);
Route::get('/ppdb/pkh/{id}', [Formulir::class, 'ppdbPkh']);
Route::get('/ppdb/foto/{id}', [Formulir::class, 'ppdbFoto']);

Route::get('/blog/{id}', [Blog::class, 'detail']);
