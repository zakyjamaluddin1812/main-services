<?php

use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Formulir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/form', [Formulir::class, 'addSiswa']);
Route::post('/form/kk/{id}', [Formulir::class, 'putKk']);
Route::post('/form/ijazah/{id}', [Formulir::class, 'putIjazah']);
Route::post('/form/nisn/{id}', [Formulir::class, 'putNisn']);
Route::post('/form/kps/{id}', [Formulir::class, 'putKps']);
Route::post('/form/pkh/{id}', [Formulir::class, 'putPkh']);
Route::post('/form/foto/{id}', [Formulir::class, 'putFoto']);
Route::post('/message', [Dashboard::class, 'sendMessage']);
