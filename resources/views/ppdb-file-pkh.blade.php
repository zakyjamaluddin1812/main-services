@extends('template')
@section('title', 'PPDB')
@section('main')
<main>
    <section id="kesiswaan" class="about">
        <div class="container">

            <div class="section-title mt-5">
                <h2>Upload PKH</h2>
                <p>Silahkan unggah file kartu pkh anda. Mohon untuk hanya mengunggah file dengan ekstensi pdf. Pastikan dokumen dapat terbaca dengan jelas. Anda dapat melewati proses ini jika dokumen belum tersedia.</p>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="row">
                        <div class="col-4">
                            <div class="position-relative img-banner text-center">
                                <span id="" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle p-2">
                                    <i id="icon" class=" text-success bi bi-cloud-arrow-up"></i>
                                    <div id="spinner" class="d-none spinner-border spinner-border-sm text-success" role="status"></div>

                                </span>
                                <img class="my-3" width="100%" id="image" src="{{asset('asets/img/error.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-8 p-1">
                            <h5>PKH</h5>
                            <p>
                                <b>Nama File : </b><br>
                                <span id="nama-file">file belum diunggah</span> <br>
                                <b>Ukuran : </b><br>
                                <span id="ukuran-file">-</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="d-flex justify-content-between">
                    <a href="/ppdb/kps/{{$id}}" class="btn btn-outline-secondary">Batal</a>
                        <a href="/ppdb/foto/{{$id}}" id="btnNext" class="btn btn-outline-success">
                            Selanjutnya
                        </a>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>

        </div>
    </section><!-- End Departments Section -->
</main>

<script>
    const sendData = () => {
        const form = document.forms[0];
        const formData = new FormData(form)
        const btnNext = document.querySelector('#btnNext')
        const spinner = document.querySelector('#spinner')
        btnNext.toggleAttribute('disabled')
        spinner.classList.toggle('d-none')



        const dataForm = {}
        for (const [key, value] of formData) {
            dataForm[key] = value
        }
        // console.log(dataForm);
        // return false

        postData('/api/form', dataForm).then((data) => {
                if (data.error) throw data
                show(data.message)
                console.log("terimakasih", data);
                window.location.href = 'ppdb/file'
            })
            .catch((err) => {
                console.log(err);
                show('data belum lengkap/tidak valid')
                btnNext.toggleAttribute('disabled')
                spinner.classList.toggle('d-none')

            })

    }

    const validasi = (e) => {
        if (e.value == '') {
            e.classList.add('is-invalid')
        } else {
            e.classList.remove('is-invalid')
        }
    }

    const validasiNik = (e) => {
        if (e.value < 1000000000000000 || e.value > 9999999999999999) {
            e.classList.add('is-invalid')
        } else {
            e.classList.remove('is-invalid')
        }
    }

    const openFile = (e) => {
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner')
        const icon = document.querySelector('#icon')
        const namaFile = document.querySelector('#nama-file')
        const ukuranFile = document.querySelector('#ukuran-file')

        spinner.classList.toggle('d-none')
        icon.classList.toggle('d-none')
        const image = document.querySelector('#image')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            const file = inputFile.files[0]
            namaFile.innerHTML = file.name
            ukuranFile.innerHTML = (parseInt(file.size) / 1000000).toFixed(2) + ' mb'
            if (file.name.split('.').pop() == 'pdf') {
                image.src = `{{asset('assets/img/pdf.png')}}`
                const url = window.location.origin + '/api/form/pkh/'
                putImage(url + `{{$id}}`, {
                        data: inputFile.files[0]
                    })
                    .then((data) => {
                        if (data.error) throw data
                        show(data.message)
                        spinner.classList.toggle('d-none')
                        icon.classList.toggle('d-none')
                    })
                    .catch((err) => {
                        show(err.error)
                        console.log(err);
                        icon.classList.toggle('d-none')
                        spinner.classList.add('d-none')
                    })
            } else {
                image.src = `{{asset('assets/img/error.png')}}`
                spinner.classList.toggle('d-none')
                icon.classList.toggle('d-none')
            }


        }
        inputFile.click()
    }
</script>
@endsection
