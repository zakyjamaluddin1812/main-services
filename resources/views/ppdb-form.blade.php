@extends('template')
@section('title', 'PPDB')
@section('main')
<main>
    <section id="kesiswaan" class="about">
        <div class="container">

            <div class="section-title mt-5">
                <h2>Isi Formulir</h2>
                <p>Silahkan isi formulir berikut untuk mengirimkan data pendaftaran peserta didik baru MTs Islamiyah Balen tahun ajaran 2023/2024. Anda mungkin memerlukan beberapa dokumen seperti kartu keluarga dan lain-lain.</p>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="icon-box">
                        <form action="/form" method="post" enctype="multipart/form-data">
                            @csrf
                            <h3 class="mt-5">Identitas Siswa</h3>
                            <div class="mb-3">
                                <label for="namaLengkap" class="form-label">Nama Lengkap <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="nama_lengkap" class="form-control" id="namaLengkap" required>
                                <div class="invalid-feedback">Nama harus dimasukkan</div>
                            </div>
                            <div class="d-flex">
                                <div class="mb-3 flex-grow-1 mr-2">
                                    <label for="ttl" class="form-label">Tempat Lahir <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="text" name="tempat_lahir" class="form-control me-1" id="ttl" required>
                                    <div class="invalid-feedback">Tempat lahir harus dimasukkan</div>
                                </div>
                                <div class="mb-3 flex-grow-1 ml-2">
                                    <label for="tgl" class="form-label">Tanggal Lahir <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="date" name="tanggal_lahir" class="form-control ms-1" id="tgl" required>
                                    <div class="invalid-feedback">Tanggal Lahir harus dimasukkan!</div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="sch" class="form-label">Asal Sekolah <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="asal_sekolah" class="form-control" id="sch" required>
                                <div class="invalid-feedback">Asal sekolah harus dimasukkan!</div>
                            </div>
                            <div class="mb-3">
                                <label for="nisn" class="form-label">Nomor Induk Siswa Nasional</label>
                                <input type="number" name="nisn" class="form-control" id="nisn">
                            </div>
                            <div class="mb-3">
                                <label for="nik" class="form-label">Nomor Induk Kependudukan <span class="text-danger">*</span></label>
                                <input onblur="validasiNik(this)" type="number" name="nik_siswa" class="form-control" id="nik" required min="1000000000000000" max="9999999999999999">
                                <div class="invalid-feedback">Masukkan NIK yang valid</div>
                            </div>
                            <div class="mb-3">
                                <label for="noKk" class="form-label">Nomor KK <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="number" name="nomor_kk" class="form-control" id="noKk" min="1000000000000000" max="9999999999999999" required>
                                <div class="invalid-feedback">Masukkan KK yang valid</div>
                            </div>

                            <div class="mb-3">
                                <label for="gender" class="form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                                <select class="custom-select" name="jenis_kelamin" aria-label="Default select example" id="gender" required>
                                    <option selected></option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                                <div class="invalid-feedback">Pilih jenis kelamin</div>
                            </div>
                            <div class="d-flex">
                                <div class="mb-3 mr-2 flex-grow-1">
                                    <label for="anakKe" class="form-label">Anak Ke <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="number" name="anak_ke" min="1" max="15" class="form-control me-1" id="anakKe" required>
                                    <div class="invalid-feedback">Masukkan angka yang valid</div>
                                </div>
                                <div class="mb-3 ml-2 flex-grow-1">
                                    <label for="saudara" class="form-label">Jumlah Saudara <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="number" name="jumlah_saudara" min="0" max="15" class="form-control ms-1" id="saudara" required>
                                    <div class="invalid-feedback">Masukkan angka yang valid</div>

                                </div>
                            </div>
                            <h3 class="mt-5">Alamat</h3>
                            <div class="d-flex">
                                <div class="mb-3 mr-2 flex-grow-1">
                                    <label for="rt" class="form-label">RT <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="number" name="rt" min="0" max="100" class="form-control me-1" id="rt" required>
                                    <div class="invalid-feedback">Masukkan angka yang valid</div>

                                </div>
                                <div class="mb-3 ml-2 flex-grow-1">
                                    <label for="rw" class="form-label">RW <span class="text-danger">*</span></label>
                                    <input onblur="validasi(this)" type="number" name="rw" min="0" max="100" class="form-control ms-1" id="rw" required>
                                    <div class="invalid-feedback">Masukkan angka yang valid</div>

                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="desa" class="form-label">Desa <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="desa" class="form-control ms-1" id="desa" required>
                                <div class="invalid-feedback">Masukkan nama desa anda</div>

                            </div>
                            <div class="mb-3">
                                <label for="kecamatan" class="form-label">Kecamatan <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="kecamatan" class="form-control ms-1" id="kecamatan" required>
                                <div class="invalid-feedback">Masukkan nama kecamatan anda</div>


                            </div>
                            <div class="mb-3">
                                <label for="kabupaten" class="form-label">Kabupaten <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="kabupaten" class="form-control ms-1" id="kabupaten" required>
                                <div class="invalid-feedback">Masukkan nama kabupaten anda</div>

                            </div>
                            <h3 class="mt-5">Identitas Orang Tua</h3>
                            <div class="mb-3">
                                <label for="namaAyah" class="form-label">Nama Ayah <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="nama_ayah" class="form-control" id="namaAyah" required>
                                <div class="invalid-feedback">Nama ayah harus diisi</div>

                            </div>
                            <div class="mb-3">
                                <label for="nikAyah" class="form-label">NIK Ayah <span class="text-danger">*</span></label>
                                <input onblur="validasiNik(this)" type="number" name="nik_ayah" min="1000000000000000" max="9999999999999999" class="form-control" id="nikAyah" required>
                                <div class="invalid-feedback">Masukkan NIK yang sesuai</div>

                            </div>
                            <div class="mb-3">
                                <label for="kerjaAyah" class="form-label">Pekerjaan Ayah <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="pekerjaan_ayah" class="form-control" id="kerjaAyah" required>
                                <div class="invalid-feedback">Pekerjaan ayah harus diisi</div>

                            </div>
                            <div class="mb-3">
                                <label for="namaIbu" class="form-label">Nama Ibu <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="nama_ibu" class="form-control" id="namaIbu" required>
                                <div class="invalid-feedback">Nama ibu harus diisi</div>

                            </div>
                            <div class="mb-3">
                                <label for="nikIbu" class="form-label">NIK Ibu <span class="text-danger">*</span></label>
                                <input onblur="validasiNik(this)" type="number" name="nik_ibu" min="1000000000000000" max="9999999999999999" class="form-control" id="nikIbu" required>
                                <div class="invalid-feedback">Masukkan NIK yang sesuai</div>

                            </div>
                            <div class="mb-3">
                                <label for="kerjaIbu" class="form-label">Pekerjaan Ibu <span class="text-danger">*</span></label>
                                <input onblur="validasi(this)" type="text" name="pekerjaan_ibu" class="form-control" id="kerjaIbu" required>
                                <div class="invalid-feedback">Pekerjaan ibu harus diisi</div>

                            </div>
                            <h3 class="mt-5">Identitas Wali</h3>
                            <div class="mb-3">
                                <label for="namaWali" class="form-label">Nama Wali</label>
                                <input type="text" name="nama_wali" class="form-control" id="namaWali">
                            </div>
                            <div class="mb-3">
                                <label for="nikWali" class="form-label">NIK Wali</label>
                                <input type="number" name="nik_wali" min="1000000000000000" max="9999999999999999" class="form-control" id="nikWali">
                            </div>
                            <div class="mb-3">
                                <label for="kerjaWali" class="form-label">Pekerjaan Wali</label>
                                <input type="text" name="pekerjaan_wali" class="form-control" id="kerjaWali">
                            </div>
                            <h3 class="mt-5">Kontak</h3>
                            <div class="mb-3">
                                <label for="noHp" class="form-label">Nomor HP Orangtua / Wali</label>
                                <input type="number" name="nomor_hp" class="form-control" id="noHp">
                            </div>

                            <h3 class="mt-5">Lain-lain</h3>
                            <div class="mb-3">
                                <label for="rekom" class="form-label">Guru Perekom</label>
                                <input type="text" name="guru_perekom" class="form-control" id="rekom">
                            </div>
                            <p>Keterangan : <br>Kolom yang wajib diisi ditandai dengan tanda bintang (<span class="text-danger">*</span>)</p>
                            <!-- <div class="row">
                                <div class="col-lg-2 col-sm-12">
                                    <div class="d-grid gap-2">
                                        <button type="submit" class="mt-5 btn btn-success">Kirim</button>
                                    </div>
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="d-flex justify-content-between">
                        <a href="/ppdb" class="btn btn-outline-secondary">Batal</a>
                        <button onclick="sendData()" id="btnNext" class="btn btn-outline-success">
                            <span id="spinner" class="d-none spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Selanjutnya
                        </button>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>

        </div>
    </section><!-- End Departments Section -->
</main>

<script>
    const sendData = () => {
        const form = document.forms[0];
        const formData = new FormData(form)
        const btnNext = document.querySelector('#btnNext')
        const spinner = document.querySelector('#spinner')
        btnNext.toggleAttribute('disabled')
        spinner.classList.toggle('d-none')



        const dataForm = {}
        for (const [key, value] of formData) {
            dataForm[key] = value
        }
        // console.log(dataForm);
        // return false

        postData('/api/form', dataForm).then((data) => {
            if (data.error) throw data
            show(data.message)
            console.log("terimakasih", data);
                const url = window.location.origin + '/ppdb/kk/'
                window.location.href = url + data.id
            })
            .catch((err) => {
                console.log(err);
                show('data belum lengkap/tidak valid')
                btnNext.toggleAttribute('disabled')
                spinner.classList.toggle('d-none')

            })

    }

    const validasi = (e) => {
        if (e.value == '') {
            e.classList.add('is-invalid')
        } else {
            e.classList.remove('is-invalid')
        }
    }

    const validasiNik = (e) => {
        if (e.value < 1000000000000000 || e.value > 9999999999999999) {
            e.classList.add('is-invalid')
        } else {
            e.classList.remove('is-invalid')
        }
    }
</script>
@endsection
