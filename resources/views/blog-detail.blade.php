@extends('template')
@section('title', $blog->title)
@section('main')
<style>
    #mark img {
        width: 100%;
    }
</style>
<main id="main">
    <br>
    <br>
    <!-- ======= Services Section ======= -->
    <section id="visi-misi" class="about">
        <div class="container">
            <div class="section-title">
                <h2>{{$blog->title}}</h2>
            </div>
            <div class="row" id="mark">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">

                    {!! str($blog->content)->markdown() !!}



                    <hr class="mt-5">
                    <div class="d-flex justify-content-between">
                        <small class="text-muted" style="font-style :italic;">Ditulis oleh : {{$blog->author}} <br>Pada : {{date('l, d M Y', strtotime($blog->created_at))}}</small>
                        <!-- <p>{{$blog->author}}, {{date('l, d M Y', strtotime($blog->created_at))}}</p> -->
                        <div class="d-block" style="width : 180px;">
                            <button onclick="copyLink()" class="float-right btn btn-success"><i class="bi bi-share mr-3"></i> Share</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>


        </div>
    </section>
</main><!-- End #main -->

<script>
    const copyLink = () => {
        navigator.clipboard.writeText(window.location.origin + '/blog/' + '{{$blog->id}}').then(() => {
            console.log('Content copied to clipboard');
            show("link telah disalin")
            /* Resolved - text copied to clipboard successfully */
        }, () => {
            console.error('Failed to copy');
            /* Rejected - text failed to copy to the clipboard */
        });
    }
</script>
@endsection
