@extends('template')
@section('title', 'Beranda')
@section('main')

<style>
    #hero {
        background: url("{{env('CMS').'assets/img/banner/'. $banner->image}}") top center;
    }

    .about .video-box {
        background: url("assets/img/profil-2.jpg") center center no-repeat;
    }
</style>
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container">
        <h1>{{$banner->title}}</h1>
        <h2>{{$banner->subtitle}}</h2>
        <a href="#about" class="btn-get-started scrollto">Get Started</a>
    </div>
</section><!-- End Hero -->

<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">

        <div class="container-fluid">

            <div class="row">
                <div data-aos="fade-up" class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch">
                </div>
                <div data-aos="fade-up" data-aos-delay="50" class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h3>Profil Madrasah</h3>
                    <p>Madrasah Tsanawiyah Islamiyah Balen berada dibawah naungan Yayasan Pondok Pesantren Roudlotut Tholibin Balen. Visi dari MTs Islamiyah Balen adalah terbentuknya siswa yang SALUT</p>

                    <div data-aos="fade-up" class="icon-box">
                        <div class="icon"><i class="ri-hand-heart-line"></i></div>
                        <h4 class="title"><a href="">Sholih</a></h4>
                        <p class="description">Mencetak santri yang sholih dengan pembiasaan sholat dhuha dan sholat dhuhur berjamaah setiap hari</p>
                    </div>

                    <div data-aos="fade-up" class="icon-box">
                        <div class="icon"><i class="ri-book-3-line"></i></div>
                        <h4 class="title"><a href="">Alim</a></h4>
                        <p class="description">Menciptakan anak ke arah peningkatan intelektual yang cerdas dan skill yang memadai baik dalam segi agama maupun ilmu pengetahuan dan teknologi </p>
                    </div>

                    <div data-aos="fade-up" class="icon-box">
                        <div class="icon"><i class="ri-medal-line"></i></div>
                        <h4 class="title"><a href="">Unggul</a></h4>
                        <p class="description">Madrasah Tsanawiyah Islamiyah Balen kerap menjuarai kompetisi/perlombaan tingkat kabupatan bahkan tingkat provinsi</p>
                    </div>
                    <div data-aos="fade-up" class="icon-box">
                        <div class="icon"><i class="bi bi-lightbulb"></i></div>
                        <h4 class="title"><a href="">Kreatif</a></h4>
                        <p class="description">Mengembangkan kreatifitas peserta didik dengan penyediaan fasilitas yang lengkap dan memadai dan pembinaan bakat melalui ekstrakuliluler yang diminati</p>
                    </div>

                </div>
            </div>

        </div>
    </section><!-- End About Section -->


    <!-- ======= Services Section ======= -->
    <section id="visi-misi" class="about">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Visi dan Misi</h2>
            </div>
            <div class="row">
                <div class="col">
                    <div data-aos="fade-up" class="icon-box">
                        <div class="icon"><i class="ri-award-fill"></i></div>
                        <h4 class="title"><a href="">Visi</a></h4>
                        <p class="description">{{$visi->value}}</p>
                    </div>
                </div>
                <div data-aos="fade-up" class="col">
                    <div class="icon-box">
                        <div class="icon"><i class="ri-award-fill"></i></div>
                        <h4 class="title"><a href="">Misi</a></h4>
                        <ul class="list-misi">
                            @foreach($misi as $m)
                            <li>{{$m->value}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Services Section -->
    <!-- ======= Services Section ======= -->
    <section id="program-unggulan" class="services">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Program Unggulan</h2>
                <p>Program unggulan bertujuan untuk memberikan kesempatan kepada peserta didik agar lebih cepat mentransfer ilmu pengetahuan dan teknologi yang diperlukan sesuai dengan bakat dan minat masing-masing dan mempersiapkan lulusan kelas unggulan menjadi peserta didik yang unggul dalam keahliannya. Terdapat 6 program unggulan yang terdapat di Madrasah Tsanawiyah Islamiyah Balen. </p>
            </div>

            <div class="row">
                @foreach($programUnggulan as $program)
                <div data-aos="fade-up" class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                    <div class="icon-box">
                        <div class="icon"><i class="{{$program->icon}}"></i></i></div>
                        <h4><a href="">{{$program->title}}</a></h4>
                        <p>{{$program->subtitle}}</p>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </section><!-- End Services Section -->

    <!-- ======= Departments Section ======= -->
    <section id="kesiswaan" class="departments">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Kesiswaan</h2>
                <p>Terdapat beberapa organisasi dan ekstrakulikuler yang terdapat di Madrasah Tsanawiyah Islamiyah Balen, antara lain PK IPNU-IPPNU, Palang Merah Remaja, dan Pramuka</p>
            </div>

            <div class="row">
                <div data-aos="fade-up" class="col-lg-3">
                    <ul class="nav nav-tabs flex-column">
                        @foreach($kesiswaan as $k)
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-{{$k->id}}">{{$k->title}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div data-aos="fade-up" class="col-lg-9 mt-4 mt-lg-0">
                    <div class="tab-content">
                        @foreach($kesiswaan as $k)
                        <div class="tab-pane active show" id="tab-{{$k->id}}">
                            <div class="row">
                                <div class="col-lg-8 details order-2 order-lg-1">
                                    <h3>{{$k->title}}</h3>
                                    <p>{{$k->content}}</p>
                                </div>
                                <div class="col-lg-4 text-center order-1 order-lg-2">
                                    <img src="{{ env('CMS') . 'assets/img/kesiswaan/' . $k->image}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Departments Section -->

    <!-- ======= Doctors Section ======= -->
    <section id="blog" class="doctors">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Blog</h2>
                <p>Kami menyimpan setiap momen atau artikel yang mungkin bermanfaat untuk dapat dibaca dan dipelajari oleh orang lain. </p>
            </div>


            <div data-aos="fade-up" class="blog owl-carousel">
                @foreach($blog as $b)
                <div onclick="detailBlog(`{{$b['id']}}`)" class="mx-3 my-3 member d-flex align-items-start">
                    <div class="pic"><img height="100%" src="{{env('CMS') . 'assets/img/blog/' . $b['thumbnail']}}" class="img-fluid" alt=""></div>
                    <div class="member-info">
                        <h4>{{$b['title']}}</h4>
                        <span>{{date('l, d M Y', strtotime($b['created_at']))}}</span>
                        <p>{{$b['deskripsi']}}</p>
                    </div>
                </div>
                @endforeach
            </div>


        </div>
    </section><!-- End Doctors Section -->


    <section id="asrama" class="about">
        <div class="container">

            <div class="section-title">
                <h2>Asrama</h2>
                <p>Pendidikan agama di Madrasah Tsanawiyah Islamiyah Balen didukung dengan adanya pondok pesantren yang tersedia untuk peserta didik. Berikut adalah pondok pesantren yang berada di lingkungan madrasah</p>
            </div>
            <div class="row">
                @foreach($asrama as $as)
                <div onclick="window.open(`{{$as->link}}`, '_blank')" class="col-lg-4 col-12 mb-3" data-aos="fade-up">
                    <div class="card" onmouseover="hover(this)" onmouseout="hoverOut(this)">
                        <div class="card-header mx-auto">
                            <div class="icon-box mt-3 ">
                                <!-- <div class="icon"><i class="ri-award-fill"></i></div> -->
                                <div class="icon"><img width="70%" src="{{env('CMS').'assets/img/asrama/'.$as->icon}}" alt="" srcset=""></div>
                            </div>
                        </div>
                        <div class="card-body text-center">
                            <h5>{{$as->title}}</h5>
                            <p class="text-muted">{{$as->subtitle}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- ======= Doctors Section ======= -->
    <section id="beasiswa" class="doctors">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Beasiswa</h2>
                <p>Terdapat beberapa beasiswa bagi peserta didik yang berprestasi atau kurang mampu. </p>
            </div>


            <div data-aos="fade-up" class="blog owl-carousel">
                @foreach($beasiswa as $b)
                <div class="mx-3 my-3 member d-flex align-items-start">
                    <div class="pic"><img src="{{env('CMS').'assets/img/beasiswa/'.$b->image}}" class="img-fluid" alt=""></div>
                    <div class="member-info">
                        <h4>{{$b->title}}.</h4>
                        <p>{{$b->content}}</p>
                    </div>
                </div>
                @endforeach

            </div>


        </div>
    </section><!-- End Doctors Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div data-aos="fade-up" class="section-title">
                <h2>Contact</h2>
                <p>Terimakasih untuk bersedia menghubungi kami untuk menyampaikan kritik dan saran dari anda. Berikut kontak kami.</p>
            </div>
        </div>

        <div data-aos="fade-up">
            <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.3943377364694!2d111.95717061414771!3d-7.1957677726292495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7787b69dc47e2d%3A0xf114b20685fd8de1!2sMTs%20Islamiyah%20Balen!5e0!3m2!1sid!2sid!4v1670205781118!5m2!1sid!2sid" frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="container">
            <div class="row mt-5">
                <div data-aos="fade-up" class="col-lg-4">
                    <div class="info">
                        @foreach($kontak as $k)
                        <div class="email">
                            <i class="{{$k->icon}}"></i>
                            <h4>{{$k->key}} :</h4>
                            <p>{{$k->value}}</p>
                        </div>
                        @endforeach
                    </div>

                </div>

                <div data-aos="fade-up" class="col-lg-8 mt-5 mt-lg-0">

                    <form action="" method="post" role="form" class="php-email-form">
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                                <input type="text" name="nama" class="form-control" id="name" placeholder="Nama anda" data-rule="minlen:4" data-msg="Masukkan setidaknya 4 karakter" />
                                <div class="invalid-feedback">
                                    Masukkan nama anda
                                </div>
                                <div class="validate"></div>
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email anda" data-rule="email" data-msg="Masukkan email yang valid" />
                                <div class="invalid-feedback">
                                    Masukkan email anda
                                </div>
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subjek" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Masukkan subject pesan" />
                            <div class="invalid-feedback">
                                Masukkan subjek pesan anda
                            </div>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="pesan" rows="5" data-rule="required" data-msg="Masukan anda sangat berarti bagi kami" placeholder="Pesan"></textarea>
                            <div class="invalid-feedback">
                                Masukkan pesan anda
                            </div>
                            <div class="validate"></div>
                        </div>
                        <div class="mb-3">
                            <div class="loading">Loading</div>
                            <div class="sent-message">Terimakasih, Pesan anda telah kami terima. </div>
                        </div>
                        <div class="text-center"><button id="btn" class="btn btn-success" onclick="submitForm()">
                                <span id="spinner" class="d-none spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Kirim Pesan
                            </button></div>
                    </form>

                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

<script>
    const submitForm = () => {
        const form = document.forms[0]
        const formData = new FormData(form)
        const btn = document.querySelector('#btn')
        const spinner = document.querySelector('#spinner')
        btn.toggleAttribute('disabled')
        spinner.classList.toggle('d-none')



        const dataForm = {}
        for (const [key, value] of formData) {
            dataForm[key] = value
        }

        postData('/api/message', dataForm).then((data) => {
                if (data.error) throw data
                show(data.message)
                console.log("terimakasih", data);
                form.pesan.value = ''
                btn.toggleAttribute('disabled')
                spinner.classList.toggle('d-none')
            })
            .catch((err) => {
                console.log(err);
                show('data belum lengkap/tidak valid')
                btn.toggleAttribute('disabled')
                spinner.classList.toggle('d-none')

            })

    }

    const detailBlog = (id) => {
        window.location.href = '/blog/'+id
    }
</script>
@endsection
