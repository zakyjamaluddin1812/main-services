@extends('template')
@section('title', 'PPDB')
@section('main')
<main>
    <section id="ppdb" class="about">
        <div class="container">

            <div data-aos="fade-up" class="section-title mt-5">
                <h2>Terimakasih</h2>
                <p>Data anda sudah terkirim <br>Silahkan screenshoot halaman ini untuk konfirmasi kepada pihak madrasah</p>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="d-flex justify-content-between">
                        <a href="/ppdb/foto/{{$id}}" class="btn btn-outline-secondary">Batal</a>
                        <a href="/" class="btn btn-outline-success">Kembali ke Home</a>

                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </section><!-- End Departments Section -->
</main><!-- End #main -->
@endsection
