@extends('template')
@section('title', 'PPDB')
@section('main')
<main>
    <section id="ppdb" class="about">
        <div class="container">

            <div data-aos="fade-up" class="section-title mt-5">
                <h2>Penerimaan Peserta Didik Baru <br>Madrasah Tsanawiyah Islamiyah Balen</h2>
                <p>Kami menyediakan fitur pendaftaran online bagi anda yang akan mendaftarkan putra/putri anda untuk sekolah di Madrasah Tsanawiyah Islamiyah Balen. Silahkan ikuti langkah-langkah berikut</p>
            </div>

            <div class="row">
                <div class="col-lg-2"></div>
                <div data-aos="fade-up" class="col">
                    <div class="icon-box">
                        <div class="icon"><i class="ri-number-1"></i></div>
                        <h3 class="title"><b>Isi Formulir</b></h3>
                        <p class="description">Lengkapi data-data yang dibutuhkan pihak sekolah, seperti identitas siswa, identitas orangtua, dan lain-lain</p>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div data-aos="fade-up" class="col">
                    <div class="icon-box">
                        <div class="icon"><i class="ri-number-2"></i></div>
                        <h3 class="title"><b>Lengkapi berkas</b></h3>
                        <p class="description">Silahkan unggah berkas-berkas yang dibutuhkan untuk kelengkapan dokumen pendaftaran. Dokumen yang dibutuhkan berupa kartu keluarga, ijazah terakhir, kartu nisn, kartu kps (jika ada), kartu pkh (jika ada), dan pas foto. Kolom ini bersifat opsional, sehingga anda bisa melewatinya jika dokumen tersebut belum tersedia</p>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div data-aos="fade-up" class="col">
                    <div class="icon-box">
                        <div class="icon"><i class="ri-number-3"></i></div>
                        <h3 class="title"><b>Konfirmasi</b></h3>
                        <p class="description">Setelah anda melakukan langkah-langkah diatas, langkah selanjutnya adalah konfirmasi kepada pihak madrasah bahwa anda telah melakukan pendaftaran secara online. Anda dapat mengirimkan bukti berupa screenshot halaman terakhir yang akan tampil ketika anda telah selesai mengisi data.</p>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div data-aos="fade-up" class="col">
                    <div class="d-flex justify-content-between">
                        <a href="index.html" class="btn btn-outline-success">Batal</a>
                        @if($status->status == 'buka')
                        <a href="/ppdb/form" class="btn btn-outline-success">Selanjutnya</a>
                        @elseif($status->status == 'tutup')
                        <div class="btn btn-secondary">Pendaftaran belum dibuka</div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>

        </div>
    </section><!-- End Departments Section -->
</main><!-- End #main -->
@endsection
