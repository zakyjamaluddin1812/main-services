<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>@yield('title') | MTs Islamiyah Balen</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    @if( $title == 'Dashboard')
    <meta property='og:title' content='Dashboard | Mts Islamiyah Balen' />
    <meta property='og:image' content="{{ asset('assets/img/thumbnail.jpg')}}" />
    <meta property='og:description' content='Profil MTs Islamiyah Balen' />
    <meta property='og:url' content='matsiba.com' />
    <!-- TYPE BELOW IS PROBABLY: 'website' or 'article' or look on https://ogp.me/#types -->
    <meta property="og:type" content='website' />
    @elseif( $title == 'PPDB')
    <meta property='og:title' content='PPDB | Mts Islamiyah Balen' />
    <meta property='og:image' content="{{ asset('assets/img/thumbnail-2.jpg')}}" />
    <meta property='og:description' content='PPDB MTs Islamiyah Balen' />
    <meta property='og:url' content='matsiba.com/ppdb' />
    <!-- TYPE BELOW IS PROBABLY: 'website' or 'article' or look on https://ogp.me/#types -->
    <meta property="og:type" content='website' />
    @elseif($title == 'Blog')
    <meta property='og:title' content='{{$blog->title}}' />
    <meta property='og:image' content="{{ env('CMS').'assets/img/blog/'.$blog->thumbnail}}" />
    <meta property='og:description' content='{{$blog->deskripsi}}' />
    <meta property='og:url' content='matsiba.com' />
    <!-- TYPE BELOW IS PROBABLY: 'website' or 'article' or look on https://ogp.me/#types -->
    <meta property="og:type" content='article' />
    @endif

    <!-- Favicons -->
    <link href="{{ asset('assets/img/logo.png')}}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

    <!-- AOS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <!-- BI -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <!-- =======================================================
  * Template Name: Medilab - v2.1.1
  * Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>


    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">
            <h1 class="logo mr-auto">
                <img src="{{ asset('assets/img/logo.png')}}" alt="" srcset="">
                <a href="/">Matsiba</a>
            </h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    @if( $title == 'Dashboard')
                    <li><a href="#hero">Home</a></li>
                    <li><a href="#visi-misi">Visi dan Misi</a></li>
                    <li><a href="#program-unggulan">Program Unggulan</a></li>
                    <li><a href="#kesiswaan">Kesiswaan</a></li>
                    <li><a href="#blog">Blog</a></li>
                    <li><a href="#asrama">Asrama</a></li>
                    <li><a href="#beasiswa">Beasiswa</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="{{ url('/ppdb#ppdb') }}"><b>PPDB</b></a></li>
                    @elseif( $title == 'PPDB')
                    <li><a href="/#hero">Home</a></li>
                    <li><a href="/#visi-misi">Visi dan Misi</a></li>
                    <li><a href="/#program-unggulan">Program Unggulan</a></li>
                    <li><a href="/#kesiswaan">Kesiswaan</a></li>
                    <li><a href="/#blog">Blog</a></li>
                    <li><a href="/#asrama">Asrama</a></li>
                    <li><a href="/#beasiswa">Beasiswa</a></li>
                    <li><a href="/#contact">Contact</a></li>
                    <li><a href="#ppdb"><b>PPDB</b></a></li>
                    @endif
                </ul>
            </nav><!-- .nav-menu -->


        </div>
    </header><!-- End Header -->

    @yield('main')

    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-6 footer-contact">
                        <h3>MTs Islamiyah Balen</h3>
                        <p>
                            Jl. PUK No. 556 <br>
                            Balenrejo, Balen, Bojonegoro<br>
                            62182 <br><br>
                            <strong>Hp:</strong> 0813 9973 1976<br>
                            <strong>Email:</strong> mtsi.balen@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-7 col-md-6 footer-links">
                        <h4>Program Unggulan</h4>
                        <div class="row">
                            <div class="col">
                                <ul>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Pendidikan Qur'an</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Salaf</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Sains</a></li>
                                </ul>
                            </div>
                            <div class="col">
                                <ul>

                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Bahasa dan Seni</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Olahraga</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="#">Informatika</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container d-md-flex py-4">

            <div class="mr-md-auto text-center text-md-left">
                <div class="copyright">
                    &copy; Copyright <strong><span>Matsiba Dev</span></strong>. All Rights Reserved
                </div>
                <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/ -->
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
            <div class="social-links text-center text-md-right pt-3 pt-md-0">
                <a target="_blank" href="https://web.facebook.com/mtsibalen" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a target="_blank" href="https://www.instagram.com/mtsislamiyahbalen/" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a target="_blank" href="https://www.youtube.com/channel/UCkyXbQa7BTKhjgY0WVU65Yw" class="linkedin"><i class="bi bi-youtube"></i></a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js')}}"></script>
    <script src="{{ asset('assets/vendor/venobox/venobox.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/counterup/counterup.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
        const blog = () => {
            window.location.replace('blog.html')
        }
    </script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js')}}"></script>
    <script src="{{ asset('assets/js/api.js')}}"></script>

</body>

</html>
