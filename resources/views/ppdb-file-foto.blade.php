@extends('template')
@section('title', 'PPDB')
@section('main')
<main>
    <section id="kesiswaan" class="about">
        <div class="container">

            <div class="section-title mt-5">
                <h2>Upload Foto</h2>
                <p>Silahkan unggah file foto anda. Mohon untuk hanya mengunggah file dengan ekstensi png, jpg atau jpeg. Pastikan foto dapat terlihat dengan jelas. Anda dapat melewati proses ini jika dokumen belum tersedia.</p>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="row">
                        <div class="col-4">
                            <div class="position-relative img-banner text-center">
                                <span id="" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle p-2">
                                    <i id="icon" class=" text-success bi bi-cloud-arrow-up"></i>
                                    <div id="spinner" class="d-none spinner-border spinner-border-sm text-success" role="status"></div>

                                </span>
                                <img class="my-3" width="100%" id="image" src="{{asset('asets/img/error.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-8 p-1">
                            <h5>FOTO</h5>
                            <p>
                                <b>Nama File : </b><br>
                                <span id="nama-file">file belum diunggah</span> <br>
                                <b>Ukuran : </b><br>
                                <span id="ukuran-file">-</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col">
                    <div class="d-flex justify-content-between">
                        <a href="/ppdb/pkh/{{$id}}" class="btn btn-outline-secondary">Batal</a>
                        <a href="/ppdb/finish/{{$id}}" id="btnNext" class="btn btn-outline-success">
                            Selanjutnya
                        </a>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>

        </div>
    </section><!-- End Departments Section -->
</main>

<script>

    const openFile = (e) => {
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner')
        const icon = document.querySelector('#icon')
        const namaFile = document.querySelector('#nama-file')
        const ukuranFile = document.querySelector('#ukuran-file')

        spinner.classList.toggle('d-none')
        icon.classList.toggle('d-none')
        const image = document.querySelector('#image')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            const file = inputFile.files[0]
            namaFile.innerHTML = file.name
            ukuranFile.innerHTML = (parseInt(file.size) / 1000000).toFixed(2) + ' mb'
            if (file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg') {
                image.src = URL.createObjectURL(inputFile.files[0])
                const url = window.location.origin + '/api/form/foto/'
                putImage(url + `{{$id}}`, {
                        data: inputFile.files[0]
                    })
                    .then((data) => {
                        if (data.error) throw data
                        show(data.message)
                        spinner.classList.toggle('d-none')
                        icon.classList.toggle('d-none')
                    })
                    .catch((err) => {
                        show(err.error)
                        console.log(err);
                        icon.classList.toggle('d-none')
                        spinner.classList.add('d-none')
                    })
            } else {
                image.src = `{{asset('assets/img/error.png')}}`
                spinner.classList.toggle('d-none')
                icon.classList.toggle('d-none')
            }


        }
        inputFile.click()
    }
</script>
@endsection
