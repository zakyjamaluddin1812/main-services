<?php

namespace App\Http\Controllers;

use App\Models\Blog as ModelsBlog;
use App\Models\BlogContent;
use Illuminate\Http\Request;

class Blog extends Controller
{
    public function detail($id)
    {
        $blog = ModelsBlog::find($id);
        $blogContent = BlogContent::where('blog_id', $id)->get();
        return view('blog-detail', [
            "title" => 'Blog',
            "blog" => $blog,
            "blogContent" => $blogContent
        ]);
    }
}
