<?php

namespace App\Http\Controllers;

use App\Models\Asrama;
use App\Models\Banner;
use App\Models\Beasiswa;
use App\Models\Blog;
use App\Models\BlogContent;
use App\Models\Kesiswaan;
use App\Models\Kontak;
use App\Models\Pesan;
use App\Models\PpdbStatus;
use App\Models\ProgramUnggulan;
use App\Models\VisiMisi;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Request;


use function PHPSTORM_META\map;

class Dashboard extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $banner = Banner::first();
        $visi = VisiMisi::where('type', 'visi')->first();
        $misi = VisiMisi::where('type', 'misi')->get();
        $programUnggulan = ProgramUnggulan::all();
        $kesiswaan = Kesiswaan::all();
        $blogs = Blog::all();
        $asrama = Asrama::all();
        $beasiswa = Beasiswa::all();
        $kontak = Kontak::all();
        return view('dashboard', [
            'title' => 'Dashboard',
            'banner' => $banner,
            'visi' => $visi,
            'misi' => $misi,
            'programUnggulan' => $programUnggulan,
            'kesiswaan' => $kesiswaan,
            'blog' => $blogs,
            'asrama' => $asrama,
            'beasiswa' => $beasiswa,
            'kontak' => $kontak,
        ]);
    }

    public function ppdb()
    {
        $status = PpdbStatus::first();
        return view('ppdb', [
            'title' => 'PPDB',
            'status' => $status
        ]);
    }

    public function sendMessage(Request $request)
    {
        $pesan = new Pesan();
        $pesan->nama = $request->post('nama');
        $pesan->email = $request->post('email');
        $pesan->subjek = $request->post('subjek');
        $pesan->pesan = $request->post('pesan');
        $pesan->save();
        return response()->json([
            "message" => "pesan anda sudah kami terima"
        ]);
    }
}
