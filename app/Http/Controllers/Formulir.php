<?php

namespace App\Http\Controllers;

use App\Models\PpdbStatus;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Formulir extends Controller
{
    public function addSiswa(Request $request)
    {
        $siswa = new Siswa();
        $nama_lengkap = $request->post('nama_lengkap');
        $tempat_lahir = $request->post('tempat_lahir');
        $tanggal_lahir = $request->post('tanggal_lahir');
        $asal_sekolah = $request->post('asal_sekolah');
        $nisn = $request->post('nisn');
        $nik_siswa = $request->post('nik_siswa');
        $nomor_kk = $request->post('nomor_kk');
        $jenis_kelamin = $request->post('jenis_kelamin');
        $anak_ke = $request->post('anak_ke');
        $jumlah_saudara = $request->post('jumlah_saudara');
        $rt = $request->post('rt');
        $rw = $request->post('rw');
        $desa = $request->post('desa');
        $kecamatan = $request->post('kecamatan');
        $kabupaten = $request->post('kabupaten');
        $nama_ayah = $request->post('nama_ayah');
        $nik_ayah = $request->post('nik_ayah');
        $pekerjaan_ayah = $request->post('pekerjaan_ayah');
        $nama_ibu = $request->post('nama_ibu');
        $nik_ibu = $request->post('nik_ibu');
        $pekerjaan_ibu = $request->post('pekerjaan_ibu');
        $nama_wali = $request->post('nama_wali');
        $nik_wali = $request->post('nik_wali');
        $pekerjaan_wali = $request->post('pekerjaan_wali');
        $nomor_hp = $request->post('nomor_hp');


        $file_kk = $request->file('file_kk');
        $file_kk_name = '';
        if ($file_kk != null) {
            if (strtolower($file_kk->extension()) == 'pdf') {
                $file_kk_name = $file_kk->hashName();
                $file_kk->move('assets/file/kk', $file_kk_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_ijazah = $request->file('file_ijazah');
        $file_ijazah_name = '';
        if ($file_ijazah != null) {
            if (strtolower($file_ijazah->extension()) == 'pdf') {
                $file_ijazah_name = $file_ijazah->hashName();
                $file_ijazah->move('assets/file/ijazah', $file_ijazah_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_nisn = $request->file('file_nisn');
        $file_nisn_name = '';
        if ($file_nisn != null) {
            if (strtolower($file_nisn->extension()) == 'pdf') {
                $file_nisn_name = $file_nisn->hashName();
                $file_nisn->move('assets/file/nisn', $file_nisn_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_kps = $request->file('file_kps');
        $file_kps_name = '';
        if ($file_kps != null) {
            if (strtolower($file_kps->extension()) == 'pdf') {
                $file_kps_name = $file_kps->hashName();
                $file_kps->move('assets/file/kps', $file_kps_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_pkh = $request->file('file_pkh');
        $file_pkh_name = '';
        if ($file_pkh != null) {
            if (strtolower($file_pkh->extension()) == 'pdf') {
                $file_pkh_name = $file_pkh->hashName();
                $file_pkh->move('assets/file/pkh', $file_pkh_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_foto = $request->file('file_foto');
        $file_foto_name = '';
        if ($file_foto != null) {
            if (strtolower($file_foto->extension()) == 'jpg' || strtolower($file_foto->extension()) == 'jpeg' || strtolower($file_foto->extension()) == 'png') {
                $file_foto_name = $file_foto->hashName();
                $file_foto->move('assets/img/foto', $file_foto_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }




        $guru_perekom = $request->post('guru_perekom');
        $siswa->tahun_ajaran = PpdbStatus::first()['tahun_ajaran'];
        $siswa->nama_lengkap = $nama_lengkap;
        $siswa->tempat_lahir = $tempat_lahir;
        $siswa->tanggal_lahir = $tanggal_lahir;
        $siswa->asal_sekolah = $asal_sekolah;
        $siswa->nisn = $nisn;
        $siswa->nik_siswa = $nik_siswa;
        $siswa->nomor_kk = $nomor_kk;
        $siswa->jenis_kelamin = $jenis_kelamin;
        $siswa->anak_ke = $anak_ke;
        $siswa->jumlah_saudara = $jumlah_saudara;
        $siswa->rt = $rt;
        $siswa->rw = $rw;
        $siswa->desa = $desa;
        $siswa->kecamatan = $kecamatan;
        $siswa->kabupaten = $kabupaten;
        $siswa->nama_ayah = $nama_ayah;
        $siswa->nik_ayah = $nik_ayah;
        $siswa->pekerjaan_ayah = $pekerjaan_ayah;
        $siswa->nama_ibu = $nama_ibu;
        $siswa->nik_ibu = $nik_ibu;
        $siswa->pekerjaan_ibu = $pekerjaan_ibu;
        $siswa->nama_wali = $nama_wali;
        $siswa->nik_wali = $nik_wali;
        $siswa->pekerjaan_wali = $pekerjaan_wali;
        $siswa->nomor_hp = $nomor_hp;
        $siswa->file_kk = $file_kk_name;
        $siswa->file_ijazah = $file_ijazah_name;
        $siswa->file_nisn = $file_nisn_name;
        $siswa->file_kps = $file_kps_name;
        $siswa->file_pkh = $file_pkh_name;
        $siswa->file_foto = $file_foto_name;
        $siswa->guru_perekom = $guru_perekom;

        $siswa->save();
        return response()->json([
            "message" => 'Data anda sudah terkirim',
            "id" => $siswa->id
        ]);
    }

    public function putKk (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'pdf') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/kk/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_kk = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit kk success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function putIjazah (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'pdf') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/ijazah/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_ijazah = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit ijazah success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
    public function putNisn (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'pdf') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/nisn/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_nisn = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit nisn success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
    public function putKps (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'pdf') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/kps/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_kps = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit kps success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
    public function putPkh (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'pdf') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/pkh/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_pkh = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit pkh success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
    public function putFoto (Request $request, $id)
    {
        $kk = $request->file('data');
        if ($kk == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($kk->extension()) == 'jpg' || strtolower($kk->extension()) == 'jpeg' || strtolower($kk->extension()) == 'png') {
            $name = $kk->hashName();
            $kk->move(public_path('assets/file/foto/'), $name);
            $siswa = Siswa::find($id);
            $siswa->file_foto = $name;
            $siswa->save();
            return response()->json([
                "message" => "edit foto success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }






    public function finish($id)
    {
        $title = 'PPDB';
        return view('ppdb-finish', [
            "title" => $title,
            'id' => $id
        ]);
    }

    public function ppdbForm()
    {
        return view('ppdb-form', [
            'title' => 'PPDB'
        ]);
    }

    public function ppdbKk($id)
    {
        return view('ppdb-file-kk', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
    public function ppdbIjazah($id)
    {
        return view('ppdb-file-ijazah', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
    public function ppdbNisn($id)
    {
        return view('ppdb-file-nisn', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
    public function ppdbKps($id)
    {
        return view('ppdb-file-kps', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
    public function ppdbPkh($id)
    {
        return view('ppdb-file-pkh', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
    public function ppdbFoto($id)
    {
        return view('ppdb-file-foto', [
            'title' => 'PPDB',
            'id' => $id
        ]);
    }
}
